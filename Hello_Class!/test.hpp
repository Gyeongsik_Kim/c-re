#ifndef __TEST_HPP__
#define __TEST_HPP__
#endif

#include <iostream>

class test{
  private:
    int a;
  public:
    test() : a(0) {};
    void hello();
    void setA(int a);
    int getA();
};
