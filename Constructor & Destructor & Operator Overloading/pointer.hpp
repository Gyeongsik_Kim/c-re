#ifndef __INTEGER_HPP__
#define __INTEGER_HPP__
#endif

#include <iostream>

class Integer{
private:
  int value;
public:
  Integer() : value(0) {
    std::cout << "MAKE!" << std::endl;
  }
  Integer(int v) : value(v) {}
  ~Integer(){
    std::cout << "DELETE!" << std::endl;
    this->value = 0;
  }
  bool operator<(int v){
    return value < v;
  }

  int getValue(){
    return this->value;
  }

  int operator=(int v){
    this->value = v;
    return this->value;
  }

  bool operator>(int v){
    return value > v;
  }

  bool operator==(int v){
    return value == v;
  }

};
