#include "pointer.hpp"
#include <iostream>

int main(){
  Integer integer;

  std::cout << integer.getValue() << std::endl;
  integer = Integer(3);
  std::cout << integer.getValue() << std::endl;
  return 0;
}
